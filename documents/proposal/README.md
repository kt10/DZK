# Proposals

> **Status**: `Draft`, `Accepted`, `Yanked`.

|ID|Name|Category|Status|
|:-:|:-:|:-:|:-:|
|[**001**](./001.md)|Staking|Consensus and Governance|`Accepted`|
