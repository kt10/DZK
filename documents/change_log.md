# Change logs

## v0.1.0 (TODO)

- Upgrade the consensus layer to a hybrid consensus system like `Babe + Grandpa`
  - Enhance security: introduce `VRF` function in block producer election process
  - Improve performance: change from real-time finality to batch finality (to be demonstrated)

## v0.0.3 (TODO)

- add implementations for the managements of staking rewards
  - calculations and distributions
- add staking related APIs
  - Query informations
  - Send transactions to do `stake`/`unstake`

## v0.0.2

- Add staking related functions
  - Governance, dynamic validator rotation, etc.
- Use custom Tendermint to support more robust validator management
  - [**DZK Tendermint**](https://github.com/haxjump/tendermint)

## v0.0.1

- Implemented an Ethereum-compatible chain based on Tendermint
