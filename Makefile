all: release

bins = ~/.cargo/dzk ~/.cargo/dzkd ~/.cargo/bin/tendermint /tmp/dzkd

export MLMMMM = /tmp/mlmmmm

define collect
	touch $(bins) && rm -rf $(bins)
	mkdir -p $(1) && rm -rf $(1)
	mkdir $(1)
	cp \
		./target/$(2)/$(1)/dzk \
		./target/$(2)/$(1)/dzkd \
		$(shell go env GOPATH)/bin/tendermint \
		$(1)/
	cd $(1) && ./dzkd pack
	rm $(1)/dzkd
	cp /tmp/dzkd $(1)/
	cp $(1)/* ~/.cargo/bin/
endef

build: tendermint
	cargo build --bins
	$(call collect,debug)

release_online: build_release_rocksdb_online

release: build_release

release_rocksdb: build_release_rocksdb

build_release_online: tendermint
	cargo build --release --bins --features="ruc_compact"
	$(call collect,release)

build_release_rocksdb_online: tendermint
	cargo build --release --bins --no-default-features --features="vsdb_rocksdb,compress,ruc_compact"
	$(call collect,release)

build_release: tendermint
	cargo build --release --bins
	$(call collect,release)

build_release_rocksdb: tendermint
	cargo build --release --bins --no-default-features --features="vsdb_rocksdb,compress"
	$(call collect,release)

build_release_musl: tendermint
	cargo build --release --bins --target=x86_64-unknown-linux-musl
	$(call collect,release,x86_64-unknown-linux-musl)

lint:
	cargo clippy
	cargo clippy --features benchmark
	cargo check --tests
	cargo check --benches

test:
	- rm -rf ~/.vsdb $(MLMMMM)
	tendermint init --home $(MLMMMM)
	cat $(MLMMMM)/config/genesis.json | jq -c | sed 's/}$$/,"app_state":{}&/g' > $(MLMMMM)/genesis.JSON
	cp $(MLMMMM)/genesis.JSON $(MLMMMM)/config/genesis.json
	cargo test -- --test-threads=1

testall: test
	- rm -rf ~/.vsdb
	cargo test \
		--no-default-features \
		--features="vsdb_rocksdb,compress" \
		-- \
		--test-threads=1

bench_release: tendermint
	cargo build --release --bins --no-default-features --features="vsdb_sled,benchmark"
	$(call collect,release)

bench_release_rocksdb: tendermint
	cargo build --release --bins --no-default-features --features="vsdb_rocksdb,compress,benchmark"
	$(call collect,release)

bench:
	- rm -rf ~/.vsdb
	cargo bench

bench_1k: stop_all bench_release
	bash tools/testnet/bench2.sh 1000

bench_10k: stop_all bench_release
	bash tools/testnet/bench2.sh 10000

bench_100k: stop_all bench_release
	bash tools/testnet/bench2.sh 100000

bench_1k_rocksdb: stop_all bench_release_rocksdb
	bash tools/testnet/bench2.sh 1000

bench_10k_rocksdb: stop_all bench_release_rocksdb
	bash tools/testnet/bench2.sh 10000

bench_100k_rocksdb: stop_all bench_release_rocksdb
	bash tools/testnet/bench2.sh 100000

fmt:
	cargo +nightly fmt

fmtall:
	bash tools/fmt.sh

clean:
	cargo clean

cleanall: clean
	git stash
	git clean -fdx

update:
	cargo update

doc:
	cargo doc --open

tendermint: submod
	-@ rm $(shell which tendermint) ~/.cargo/bin/dzk*
	cd tools/submodules/tendermint && $(MAKE) install

submod:
	git submodule update --init --recursive

# create an initial snaphsot for remote environments
prodenv: stop_all build_release_online
	PRODENV_NODE_CNT=7 PRODENV_BLOCK_ITV=3 bash tools/testnet/create_prod_env.sh

# create an initial snaphsot for remote environments
prodenv_rocksdb: stop_all build_release_rocksdb_online
	PRODENV_NODE_CNT=7 PRODENV_BLOCK_ITV=3 bash tools/testnet/create_prod_env.sh

stop_all:
	- pkill -9 tendermint
	- pkill -9 dzk

######################################################################

localnet_run:
	bash tools/testnet/create_prod_env.sh default
	dzk dev -s

localnet_start:
	dzk dev -s

localnet_stop:
	dzk dev -S

localnet_destroy: localnet_stop
	dzk dev -d

localnet_show:
	dzk dev -i

testnet_join_node: testnet_stop_node release
	bash tools/testnet/add_fullnode.sh testnet

testnet_start_node:
	bash tools/testnet/run_fullnode.sh

testnet_stop_node:
	bash tools/testnet/kill_fullnode.sh

mainnet_join_node: mainnet_stop_node release
	bash tools/testnet/add_fullnode.sh mainnet

mainnet_start_node: testnet_start_node

mainnet_stop_node: testnet_stop_node

######################################################################

# CI 
ci_build_binary_rust_base:
	docker build -t binary-rust-base -f container/Dockerfile-binary-rust-base .

ci_build_dev_binary_image:
	sed -i "s/^ENV VERGEN_SHA_EXTERN .*/ENV VERGEN_SHA_EXTERN ${VERGEN_SHA_EXTERN}/g" container/Dockerfile-binary-image-release
	docker build -t dzkd-binary-image:$(IMAGE_TAG) -f container/Dockerfile-binary-image-dev .
	
ci_build_release_binary_image:
	sed -i "s/^ENV VERGEN_SHA_EXTERN .*/ENV VERGEN_SHA_EXTERN ${VERGEN_SHA_EXTERN}/g" container/Dockerfile-binary-image-release
	docker build -t dzkd-binary-image:$(IMAGE_TAG) -f container/Dockerfile-binary-image-release .

ci_build_image:
	@ if [ -d "./binary" ]; then \
		rm -rf ./binary || true; \
	fi
	@ docker run --rm -d --name dzkd-binary dzkd-binary-image:$(IMAGE_TAG)
	@ docker cp dzkd-binary:/binary ./binary
	@ docker rm -f dzkd-binary
	@ docker build -t $(PUBLIC_ECR_URL)/$(ENV)/dzkd:$(IMAGE_TAG) -f container/Dockerfile-goleveldb .
ifeq ($(ENV),release)
	docker tag $(PUBLIC_ECR_URL)/$(ENV)/dzkd:$(IMAGE_TAG) $(PUBLIC_ECR_URL)/$(ENV)/dzkd:latest
endif

ci_push_image:
	docker push $(PUBLIC_ECR_URL)/$(ENV)/dzkd:$(IMAGE_TAG)
ifeq ($(ENV),release)
	docker push $(PUBLIC_ECR_URL)/$(ENV)/dzkd:latest
endif

clean_image:
	docker rmi $(PUBLIC_ECR_URL)/$(ENV)/dzkd:$(IMAGE_TAG)
ifeq ($(ENV),release)
	docker rmi $(PUBLIC_ECR_URL)/$(ENV)/dzkd:latest
endif

clean_binary_image:
	docker rmi dzkd-binary-image:$(IMAGE_TAG)
