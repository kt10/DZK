//!
//! # Transaction logic
//!

pub mod native;

use crate::{common::HashValue, ethvm};
use ruc::*;
use serde::{Deserialize, Serialize};
use vsdb::{hash, ValueEnDe};

#[non_exhaustive]
#[allow(clippy::large_enum_variant)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Tx {
    Evm(ethvm::tx::Tx),
    Native(native::Tx),
}

impl Tx {
    #[inline(always)]
    pub(crate) fn hash(&self) -> HashValue {
        hash(&[&self.encode()]).to_vec()
    }

    #[inline(always)]
    pub(crate) fn deserialize(bytes: &[u8]) -> Result<Tx> {
        Tx::decode(bytes).c(d!())
    }

    // TODO
    #[inline(always)]
    pub(crate) fn valid_in_abci(&self) -> bool {
        true
    }
}
