use jsonrpc_core::{Error, ErrorCode};

#[inline(always)]
pub fn no_impl() -> Error {
    Error {
        code: ErrorCode::ServerError(40001),
        message: String::from("No impl."),
        data: None,
    }
}

#[inline(always)]
pub fn new_jsonrpc_error(msg: impl AsRef<str>) -> Error {
    Error {
        code: ErrorCode::ServerError(40002),
        message: msg.as_ref().to_owned(),
        data: None,
    }
}
