use crate::{
    common::{
        block_hash_to_height, block_number_to_height, hash_to_evm_format,
        tm_proposer_to_evm_format, tm_rpc_get, tm_rpc_post, BlockHeight,
    },
    ledger::{State, VsVersion, MAIN_BRANCH_NAME, RECEIPT_CACHE},
    rpc::{
        error::{new_jsonrpc_error, no_impl},
        utils::{filter_block_logs, tx_to_web3_tx, txs_to_web3_txs},
    },
    tx::Tx,
    EvmTx,
};
use byte_slice_cast::AsByteSlice;
use ethereum::TransactionAny;
use ethereum_types::{Bloom, H160, H256, H64, U256, U64};
use jsonrpc_core::{BoxFuture, Result};
use rlp::{Decodable, Rlp};
use serde_json::Value;
use vsdb::ValueEn;
use web3_rpc_core::{
    types::{
        Block, BlockNumber, BlockTransactions, Bytes, CallRequest, Filter,
        FilteredParams, Index, Log, Receipt, RichBlock, SyncInfo, SyncStatus,
        Transaction, TransactionRequest, Work,
    },
    EthApi,
};

const BASE_GAS: u64 = 21_000;

pub(crate) struct EthApiImpl {
    pub upstream: String,
    pub upstream_is_unix_sock: bool,
    pub state: State,
}

impl EthApi for EthApiImpl {
    fn protocol_version(&self) -> BoxFuture<Result<u64>> {
        Box::pin(async move { Ok(65) })
    }

    fn chain_id(&self) -> BoxFuture<Result<Option<U64>>> {
        let chain_id = self.state.chain_id.get_value().unwrap();
        Box::pin(async move { Ok(Some(U64::from(chain_id))) })
    }

    fn balance(
        &self,
        address: H160,
        bn: Option<BlockNumber>,
    ) -> BoxFuture<Result<U256>> {
        let height = block_number_to_height(bn, &self.state.evm);
        let ver = VsVersion::new_with_default_mark(height, 0);
        let account = self.state.evm.token.accounts.get_by_branch_version(
            &address,
            MAIN_BRANCH_NAME,
            ver.encode_value().as_ref().into(),
        );
        let balance = account.map(|x| x.balance).unwrap_or(U256::zero());

        Box::pin(async move { Ok(balance) })
    }

    // Low priority, not implemented for now
    fn send_transaction(&self, _: TransactionRequest) -> BoxFuture<Result<H256>> {
        // Cal tendermint send tx.

        Box::pin(async { Ok(H256::default()) })
    }

    fn call(
        &self,
        req: CallRequest,
        bn: Option<BlockNumber>,
    ) -> BoxFuture<Result<Bytes>> {
        let r;
        let resp = self
            .state
            .evm
            .contract_handle(MAIN_BRANCH_NAME, req, bn)
            .map_err(|e| new_jsonrpc_error(e.to_string()));

        ruc::d!(format!("{:?}", resp));

        if let Err(e) = resp {
            r = Err(e)
        } else if let Ok(resp) = resp {
            let bytes = Bytes::new(resp.data);
            r = Ok(bytes)
        } else {
            r = Ok(Bytes::default())
        }

        Box::pin(async { r })
    }

    fn syncing(&self) -> BoxFuture<Result<SyncStatus>> {
        let upstream = self.upstream.clone();
        let is_unix = self.upstream_is_unix_sock;
        Box::pin(async move {
            let (status_code, resp) = tm_rpc_get(&upstream, "/status", &[], is_unix)
                .map_err(|e| new_jsonrpc_error(e.to_string()))?;
            if 200 != status_code {
                return Err(new_jsonrpc_error(&String::from_utf8_lossy(&resp)));
            }
            let resp = serde_json::to_value(&resp)
                .map_err(|e| new_jsonrpc_error(e.to_string()))?;
            if let Some(result) = resp.get("result") {
                // The following unwrap operations are safe
                // If the return result field of tendermint remains unchanged
                let sync_info = result.get("sync_info").unwrap();
                let catching_up =
                    sync_info.get("catching_up").unwrap().as_bool().unwrap();
                if catching_up {
                    Ok(SyncStatus::Info(SyncInfo {
                        starting_block: Default::default(),
                        current_block: U256::from(
                            sync_info
                                .get("latest_block_height")
                                .unwrap()
                                .to_string()
                                .as_bytes(),
                        ),
                        highest_block: Default::default(),
                        warp_chunks_amount: None,
                        warp_chunks_processed: None,
                    }))
                } else {
                    Ok(SyncStatus::None)
                }
            } else {
                Err(new_jsonrpc_error(resp.to_string()))
            }
        })
    }

    fn author(&self) -> BoxFuture<Result<H160>> {
        // current proposer
        let current_proposer = self.state.evm.vicinity.block_coinbase;

        Box::pin(async move { Ok(current_proposer) })
    }

    fn is_mining(&self) -> BoxFuture<Result<bool>> {
        // is validator?

        Box::pin(async move { Ok(false) })
    }

    fn gas_price(&self) -> BoxFuture<Result<U256>> {
        let gas_price = self.state.evm.gas_price.get_value().unwrap();

        Box::pin(async move { Ok(gas_price) })
    }

    fn block_number(&self) -> BoxFuture<Result<U256>> {
        // return current latest block number.

        let r = if let Some((height, _)) = self.state.blocks.last() {
            Ok(U256::from(height))
        } else {
            Err(new_jsonrpc_error("state blocks is none"))
        };

        Box::pin(async move { r })
    }

    fn storage_at(
        &self,
        addr: H160,
        index: U256,
        bn: Option<BlockNumber>,
    ) -> BoxFuture<Result<H256>> {
        // TODO: I'm not sure if this is the right thing to do
        let key = (&addr, &H256::from_slice(index.as_byte_slice()));

        let height = block_number_to_height(bn, &self.state.evm);
        let ver = VsVersion::new_with_default_mark(height, 0);

        let storages = self.state.evm.token.storages.get_by_branch_version(
            &key,
            MAIN_BRANCH_NAME,
            ver.encode_value().as_ref().into(),
        );

        let val = storages.unwrap_or_default();

        Box::pin(async move { Ok(val) })
    }

    fn block_by_hash(
        &self,
        block_hash: H256,
        is_complete: bool,
    ) -> BoxFuture<Result<Option<RichBlock>>> {
        let mut op_rb = None;
        let height = block_hash_to_height(block_hash, &self.state.evm);
        if let Some(block) = self.state.blocks.get(&height) {
            let chain_id = self.state.chain_id.get_value().unwrap();

            let proposer = tm_proposer_to_evm_format(&block.header.proposer);

            let receipt = if let Some((_, receipt)) = block.header.receipts.iter().last()
            {
                receipt.clone()
            } else {
                Default::default()
            };

            // prev is null if block is 1
            let parent_hash = if block.header.prev_hash.is_empty() {
                H256::default()
            } else {
                hash_to_evm_format(&block.header.prev_hash)
            };

            let web3_txs = match txs_to_web3_txs(&block, chain_id, height) {
                Ok(v) => v,
                Err(e) => return Box::pin(async { Err(e) }),
            };

            // Determine if you want to return all block information
            let transactions = if is_complete {
                BlockTransactions::Full(web3_txs)
            } else {
                let tx_hashes = web3_txs.iter().map(|t| t.hash).collect::<Vec<H256>>();
                BlockTransactions::Hashes(tx_hashes)
            };

            let b = Block {
                hash: Some(hash_to_evm_format(&block.header_hash)),
                parent_hash,
                uncles_hash: Default::default(),
                author: proposer,
                miner: proposer,
                state_root: Default::default(),
                transactions_root: hash_to_evm_format(&block.header.tx_trie_root),
                receipts_root: Default::default(),
                number: Some(U256::from(height)),
                gas_used: receipt.block_gas_used,
                gas_limit: self.state.evm.block_gas_limit.get_value().unwrap(),
                extra_data: Default::default(),
                logs_bloom: Some(Bloom::from_slice(block.bloom.as_slice())),
                timestamp: U256::from(block.header.timestamp),
                difficulty: Default::default(),
                total_difficulty: Default::default(),
                seal_fields: vec![],
                uncles: vec![],
                transactions,
                size: Some(U256::from(
                    serde_json::to_vec(&block).unwrap_or_default().len(),
                )),
            };

            op_rb.replace(RichBlock {
                inner: b,
                extra_info: Default::default(),
            });
        }

        Box::pin(async { Ok(op_rb) })
    }

    fn block_by_number(
        &self,
        bn: BlockNumber,
        is_complete: bool,
    ) -> BoxFuture<Result<Option<RichBlock>>> {
        let height = block_number_to_height(Some(bn), &self.state.evm);

        let op = if let Some(block) = self.state.blocks.get(&height) {
            let proposer = tm_proposer_to_evm_format(&block.header.proposer);

            // prev is null if block is 1
            let parent_hash = if block.header.prev_hash.is_empty() {
                H256::default()
            } else {
                hash_to_evm_format(&block.header.prev_hash)
            };

            let receipt = if let Some((_, receipt)) = block.header.receipts.iter().last()
            {
                receipt.clone()
            } else {
                Default::default()
            };

            let chain_id = self.state.chain_id.get_value().unwrap();
            let web3_txs = match txs_to_web3_txs(&block, chain_id, height) {
                Ok(v) => v,
                Err(e) => {
                    return Box::pin(async { Err(e) });
                }
            };

            // Determine if you want to return all block information
            let transactions = if is_complete {
                BlockTransactions::Full(web3_txs)
            } else {
                let tx_hashes = web3_txs.iter().map(|t| t.hash).collect::<Vec<H256>>();
                BlockTransactions::Hashes(tx_hashes)
            };

            let b = Block {
                hash: Some(hash_to_evm_format(&block.header_hash)),
                parent_hash,
                uncles_hash: Default::default(),
                author: proposer,
                miner: proposer,
                state_root: Default::default(),
                transactions_root: hash_to_evm_format(&block.header.tx_trie_root),
                receipts_root: Default::default(),
                number: Some(U256::from(height)),
                gas_used: receipt.block_gas_used,
                gas_limit: self.state.evm.block_gas_limit.get_value().unwrap(),
                extra_data: Default::default(),
                logs_bloom: Some(Bloom::from_slice(&block.bloom)),
                timestamp: U256::from(block.header.timestamp),
                difficulty: Default::default(),
                total_difficulty: Default::default(),
                seal_fields: vec![],
                uncles: vec![],
                transactions,
                size: Some(U256::from(
                    serde_json::to_vec(&block).unwrap_or_default().len(),
                )),
            };

            Some(RichBlock {
                inner: b,
                extra_info: Default::default(),
            })
        } else {
            None
        };

        Box::pin(async { Ok(op) })
    }

    fn transaction_count(
        &self,
        addr: H160,
        bn: Option<BlockNumber>,
    ) -> BoxFuture<Result<U256>> {
        let height = block_number_to_height(bn, &self.state.evm);
        let ver = VsVersion::new_with_default_mark(height, 0);

        let nonce = self
            .state
            .evm
            .token
            .accounts
            .get_by_branch_version(
                &addr,
                MAIN_BRANCH_NAME,
                ver.encode_value().as_ref().into(),
            )
            .map(|x| x.nonce)
            .unwrap_or(U256::zero());

        Box::pin(async move { Ok(nonce) })
    }

    fn block_transaction_count_by_hash(
        &self,
        block_hash: H256,
    ) -> BoxFuture<Result<Option<U256>>> {
        let height = block_hash_to_height(block_hash, &self.state.evm);

        let tx_count = self
            .state
            .blocks
            .get(&height)
            .map(|block| block.txs.len())
            .unwrap_or(0);

        Box::pin(async move { Ok(Some(U256::from(tx_count))) })
    }

    fn block_transaction_count_by_number(
        &self,
        bn: BlockNumber,
    ) -> BoxFuture<Result<Option<U256>>> {
        let height = block_number_to_height(Some(bn), &self.state.evm);

        let tx_count = if let Some(block) = self.state.blocks.get(&height) {
            block.txs.len()
        } else {
            Default::default()
        };
        Box::pin(async move { Ok(Some(U256::from(tx_count))) })
    }

    fn code_at(&self, addr: H160, bn: Option<BlockNumber>) -> BoxFuture<Result<Bytes>> {
        let height = block_number_to_height(bn, &self.state.evm);
        let ver = VsVersion::new_with_default_mark(height, 0);

        let account = self.state.evm.token.accounts.get_by_branch_version(
            &addr,
            MAIN_BRANCH_NAME,
            ver.encode_value().as_ref().into(),
        );

        let bytes = account
            .map(|x| x.code)
            .or_else(|| Some(Default::default()))
            .unwrap();

        Box::pin(async { Ok(Bytes::new(bytes)) })
    }

    fn send_raw_transaction(&self, tx: Bytes) -> BoxFuture<Result<H256>> {
        let evm_tx = match TransactionAny::decode(&Rlp::new(tx.0.as_slice())) {
            Ok(t) => t,
            Err(e) => {
                return Box::pin(async move { Err(new_jsonrpc_error(e.to_string())) });
            }
        };

        let tx = Tx::Evm(EvmTx { tx: evm_tx });
        let req_body = format!(
            r#"{{"id":0,"method":"broadcast_tx_async","params": {{"tx":"{}"}}}}"#,
            &base64::encode(tx.encode_value())
        )
        .into_bytes();
        let upstream = self.upstream.clone();
        let is_unix = self.upstream_is_unix_sock;
        Box::pin(async move {
            let (status_code, resp) = tm_rpc_post(&upstream, req_body, is_unix)
                .map_err(|e| new_jsonrpc_error(e.to_string()))?;
            if 200 != status_code {
                return Err(new_jsonrpc_error(&String::from_utf8_lossy(&resp)));
            }
            let resp = serde_json::from_slice::<Value>(&resp)
                .map_err(|e| new_jsonrpc_error(e.to_string()))?;
            if let Some(result) = resp.get("result") {
                if let Some(code) = result.get("code") {
                    if code.eq(&0) {
                        return Ok(hash_to_evm_format(&tx.hash()));
                    }
                }
            }
            Err(new_jsonrpc_error(resp.to_string()))
        })
    }

    fn estimate_gas(
        &self,
        req: CallRequest,
        bn: Option<BlockNumber>,
    ) -> BoxFuture<Result<U256>> {
        let r;
        let resp = self
            .state
            .evm
            .contract_handle(MAIN_BRANCH_NAME, req, bn)
            .map_err(|e| new_jsonrpc_error(e.to_string()));

        ruc::d!(format!("{:?}", resp));

        if let Err(e) = resp {
            r = Err(e)
        } else if let Ok(resp) = resp {
            let gas_used = U256::from(resp.gas_used + BASE_GAS);
            r = Ok(gas_used)
        } else {
            r = Err(new_jsonrpc_error("call contract resp none"));
        }

        Box::pin(async { r })
    }

    fn transaction_by_hash(
        &self,
        tx_hash: H256,
    ) -> BoxFuture<Result<Option<Transaction>>> {
        let mut transaction = None;

        if let Some((height, index)) = self.state.tx_hash_index.get(&tx_hash) {
            if let Some(block) = self.state.blocks.get(&height) {
                if let Some(tx) = block.txs.get(index) {
                    match tx_to_web3_tx(
                        &tx,
                        &block,
                        height,
                        index,
                        self.state.chain_id.get_value().unwrap(),
                    ) {
                        Ok(op) => {
                            transaction = op;
                        }
                        Err(e) => {
                            return Box::pin(async { Err(e) });
                        }
                    }
                }
            }
        }

        Box::pin(async { Ok(transaction) })
    }

    fn transaction_by_block_hash_and_index(
        &self,
        block_hash: H256,
        index: Index,
    ) -> BoxFuture<Result<Option<Transaction>>> {
        let mut transaction = None;

        if let Some(height) = self
            .state
            .evm
            .block_hash_index
            .get(block_hash.as_bytes().to_vec().as_ref())
        {
            if let Some(block) = self.state.blocks.get(&height) {
                if let Some(tx) = block.txs.get(index.value()) {
                    match tx_to_web3_tx(
                        &tx,
                        &block,
                        height,
                        index.value(),
                        self.state.chain_id.get_value().unwrap(),
                    ) {
                        Ok(op) => {
                            transaction = op;
                        }
                        Err(e) => {
                            return Box::pin(async { Err(e) });
                        }
                    }
                }
            }
        }
        Box::pin(async { Ok(transaction) })
    }

    fn transaction_by_block_number_and_index(
        &self,
        bn: BlockNumber,
        index: Index,
    ) -> BoxFuture<Result<Option<Transaction>>> {
        let height = block_number_to_height(Some(bn), &self.state.evm);
        let mut transaction = None;

        if let Some(block) = self.state.blocks.get(&height) {
            if let Some(tx) = block.txs.get(index.value()) {
                match tx_to_web3_tx(
                    &tx,
                    &block,
                    height,
                    index.value(),
                    self.state.chain_id.get_value().unwrap(),
                ) {
                    Ok(op) => {
                        transaction = op;
                    }
                    Err(e) => {
                        return Box::pin(async { Err(e) });
                    }
                }
            }
        }

        Box::pin(async { Ok(transaction) })
    }

    fn transaction_receipt(&self, tx_hash: H256) -> BoxFuture<Result<Option<Receipt>>> {
        let info = if let Some(ret) = RECEIPT_CACHE.read().get(&tx_hash).cloned() {
            Some(ret)
        } else if let Some((height, _)) = self.state.tx_hash_index.get(&tx_hash) {
            let mut ret = None;
            if let Some(block) = self.state.blocks.get(&height) {
                if let Some(r) = block
                    .header
                    .receipts
                    .get(tx_hash.as_bytes().to_vec().as_slice())
                {
                    let block_hash = hash_to_evm_format(&block.header_hash);
                    ret = Some((height, block_hash, r.clone()));
                }
            }
            ret
        } else {
            None
        };

        let ret = if let Some((height, block_hash, r)) = info {
            let mut logs = vec![];
            for l in r.logs.iter() {
                logs.push(Log {
                    address: l.address,
                    topics: l.topics.clone(),
                    data: Bytes::new(l.data.clone()),
                    block_hash: Some(block_hash),
                    block_number: Some(U256::from(height)),
                    transaction_hash: Some(tx_hash),
                    transaction_index: Some(U256::from(l.tx_index)),
                    log_index: Some(U256::from(l.log_index_in_block)),
                    transaction_log_index: Some(U256::from(l.log_index_in_tx)),
                    removed: false,
                });
            }

            let status_code = if BlockHeight::MAX == height || !r.success {
                Some(U64::from(0u8))
            } else {
                Some(U64::from(1u8))
            };
            Some(Receipt {
                transaction_hash: Some(tx_hash),
                transaction_index: Some(U256::from(r.tx_index)),
                block_hash: Some(block_hash),
                from: r.from,
                to: r.to,
                block_number: Some(U256::from(height)),
                cumulative_gas_used: r.block_gas_used,
                gas_used: Some(r.tx_gas_used),
                contract_address: r.contract_addr,
                logs,
                state_root: None,
                logs_bloom: Default::default(),
                status_code,
            })
        } else {
            None
        };

        Box::pin(async { Ok(ret) })
    }

    fn logs(&self, filter: Filter) -> BoxFuture<Result<Vec<Log>>> {
        let mut logs = vec![];

        if let Some(hash) = filter.block_hash {
            if let Some(height) = self
                .state
                .evm
                .block_hash_index
                .get(hash.as_bytes().to_vec().as_ref())
            {
                if let Some(block) = self.state.blocks.get(&height) {
                    logs.append(&mut filter_block_logs(&block, &filter, height));
                }
            }
        } else {
            let (current_height, _) = self.state.blocks.last().unwrap_or_default();

            let mut to =
                block_number_to_height(filter.to_block.clone(), &self.state.evm);
            if to > current_height {
                to = current_height;
            }

            let mut from =
                block_number_to_height(filter.from_block.clone(), &self.state.evm);
            if from > current_height {
                from = current_height;
            }

            let topics_input = if filter.topics.is_some() {
                let filtered_params = FilteredParams::new(Some(filter.clone()));
                Some(filtered_params.flat_topics)
            } else {
                None
            };

            let address_bloom_filter =
                FilteredParams::addresses_bloom_filter(&filter.address);
            let topic_bloom_filters = FilteredParams::topics_bloom_filter(&topics_input);

            for height in from..=to {
                if let Some(block) = self.state.blocks.get(&height) {
                    let b = Bloom::from_slice(block.bloom.as_slice());
                    if FilteredParams::address_in_bloom(b, &address_bloom_filter)
                        && FilteredParams::topics_in_bloom(b, &topic_bloom_filters)
                    {
                        logs.append(&mut filter_block_logs(&block, &filter, height));
                    }
                }
            }
        };

        Box::pin(async { Ok(logs) })
    }

    // ----------- Not impl.
    fn work(&self) -> Result<Work> {
        Err(no_impl())
    }

    fn submit_work(&self, _: H64, _: H256, _: H256) -> Result<bool> {
        Err(no_impl())
    }

    fn submit_hashrate(&self, _: U256, _: H256) -> Result<bool> {
        Err(no_impl())
    }

    fn hashrate(&self) -> Result<U256> {
        Err(no_impl())
    }
    fn uncle_by_block_hash_and_index(
        &self,
        _: H256,
        _: Index,
    ) -> Result<Option<RichBlock>> {
        Err(no_impl())
    }

    fn uncle_by_block_number_and_index(
        &self,
        _: BlockNumber,
        _: Index,
    ) -> Result<Option<RichBlock>> {
        Err(no_impl())
    }

    fn block_uncles_count_by_hash(&self, _: H256) -> Result<U256> {
        Err(no_impl())
    }

    fn block_uncles_count_by_number(&self, _: BlockNumber) -> Result<U256> {
        Err(no_impl())
    }

    fn accounts(&self) -> Result<Vec<H160>> {
        // This api is no impl, only return a empty array.
        Ok(vec![])
    }
}
