#![deny(warnings)]

use clap::Parser;
use dzk::{Cfg, Commands};
use once_cell::sync::Lazy;
use ruc::*;
use tokio::runtime::Runtime;

mod client;
pub mod daemon;
mod dev;

#[cfg(target_os = "linux")]
mod snapshot;

static RT: Lazy<Runtime> = Lazy::new(|| pnk!(Runtime::new()));

fn main() {
    let config = Cfg::parse();

    match config.commands {
        Commands::Cli(cfg) => {
            pnk!(RT.block_on(client::exec(cfg)));
        }
        Commands::Daemon(cfg) => {
            pnk!(daemon::exec(cfg));
        }
        Commands::Dev(cfg) => {
            pnk!(dev::EnvCfg::from(cfg).exec());
        }

        #[cfg(target_os = "linux")]
        Commands::Snap(cfg) => {
            pnk!(snapshot::exec(cfg));
        }
    }
}
