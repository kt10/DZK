#![deny(warnings)]

use clap::{Parser, Subcommand};
use dzk::DaemonCfg;
use ruc::*;

mod daemon;
mod pack;

#[derive(Parser, Debug)]
#[clap(about, version, author)]
pub struct Cfg {
    #[clap(subcommand)]
    pub commands: Commands,
}

#[derive(Debug, Subcommand)]
#[allow(clippy::large_enum_variant)]
pub enum Commands {
    #[clap(about = "Run dzk in daemon mode")]
    Daemon(DaemonCfg),
    #[clap(about = "An alias of `dzk daemon`")]
    Node(DaemonCfg),
    #[clap(about = "Pack tendermint into the dzkd binary")]
    Pack,
    #[clap(about = "Unpack tendermint from the dzkd binary")]
    Unpack,
}

fn main() {
    let config = Cfg::parse();

    match config.commands {
        Commands::Daemon(cfg) | Commands::Node(cfg) => {
            pnk!(pack::unpack());
            pnk!(daemon::dzkd_exec(cfg));
        }
        Commands::Pack => {
            pnk!(pack::pack());
        }
        Commands::Unpack => {
            pnk!(pack::unpack());
        }
    }
}
