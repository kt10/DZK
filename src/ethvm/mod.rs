pub mod impls;
mod precompile;
pub mod tx;

use crate::common::HashValue;
use crate::{
    common::{block_number_to_height, BlockHeight},
    ethvm::precompile::PRECOMPILE_SET,
    ledger::{VsVersion, MAIN_BRANCH_NAME},
};
use ethereum_types::{H160, H256, U256};
use evm::{
    executor::stack::{MemoryStackState, StackExecutor, StackSubstateMetadata},
    ExitReason,
};
use impls::backend::DzkBackend;
use once_cell::sync::Lazy;
use ruc::*;
use serde::{Deserialize, Serialize};
use tx::token::Erc20Like;
use vsdb::{BranchName, MapxOrd, OrphanVs, ParentBranchName, ValueEn, Vs, VsMgmt};
use web3_rpc_core::types::{BlockNumber, CallRequest};

#[allow(non_snake_case)]
#[derive(Vs, Clone, Debug, Deserialize, Serialize)]
pub struct State {
    pub gas_price: OrphanVs<U256>,
    pub block_gas_limit: OrphanVs<U256>,
    pub block_base_fee_per_gas: OrphanVs<U256>,

    pub token: Erc20Like,

    // Environmental block hashes.
    pub block_hashes: MapxOrd<BlockHeight, H256>,

    // Oneshot values for each evm transaction.
    pub vicinity: DzkVicinity,

    pub block_hash_index: MapxOrd<HashValue, BlockHeight>,
}

impl State {
    pub fn new() -> Self {
        Self {
            gas_price: OrphanVs::new(),
            block_gas_limit: OrphanVs::new(),
            block_base_fee_per_gas: OrphanVs::new(),
            token: Erc20Like::default(),
            block_hashes: MapxOrd::new(),
            vicinity: DzkVicinity::default(),
            block_hash_index: MapxOrd::new(),
        }
    }

    pub fn contract_handle(
        &self,
        branch_name: BranchName,
        req: CallRequest,
        bn: Option<BlockNumber>,
    ) -> Result<CallContractResp> {
        static U64_MAX: Lazy<U256> = Lazy::new(|| U256::from(u64::MAX));

        // Operation Type
        enum Operation {
            Call,
            Create,
        }

        // Determine what type of operation is being performed based on the parameter to in the request object
        let (operation, address) = if let Some(to) = req.to {
            (Operation::Call, to)
        } else {
            (Operation::Create, H160::default())
        };

        let caller = req.from.unwrap_or_default();
        let value = req.value.unwrap_or_default();
        let data = req.data.unwrap_or_default();

        // This parameter is used as the divisor and cannot be 0
        let gas = if let Some(gas) = req.gas {
            alt!(gas > *U64_MAX, u64::MAX, gas.as_u64())
        } else {
            u64::MAX
        };
        let gas_price = req.gas_price.unwrap_or_else(U256::one);
        let gas_price = alt!(gas_price > *U64_MAX, u64::MAX, gas_price.as_u64());
        let gas_limit = gas.checked_div(gas_price).unwrap(); //safe

        let height = block_number_to_height(bn, self);

        let branch_tmp = snapshot_at_height(height, self, "call_contract").c(d!())?;

        let backend = DzkBackend {
            branch: branch_name,
            state: self.token.accounts.clone(),
            storages: self.token.storages.clone(),
            block_hashes: self.block_hashes,
            vicinity: self.vicinity.clone(),
        };

        let cfg = evm::Config::istanbul();
        let metadata = StackSubstateMetadata::new(u64::MAX, &cfg);

        let dzk_stack_state = MemoryStackState::new(metadata, &backend);
        let precompiles = PRECOMPILE_SET.clone();
        let mut executor =
            StackExecutor::new_with_precompiles(dzk_stack_state, &cfg, &precompiles);

        let resp = match operation {
            Operation::Call => {
                executor.transact_call(caller, address, value, data.0, gas_limit, vec![])
            }
            Operation::Create => {
                executor.transact_create(caller, value, data.0, gas_limit, vec![])
            }
        };

        d!(format!("{:?}", resp));

        let cc_resp = CallContractResp {
            evm_resp: resp.0,
            data: resp.1,
            gas_used: executor.used_gas(),
        };

        self.branch_remove(BranchName::from(branch_tmp.as_str()))
            .c(d!())?;

        Ok(cc_resp)
    }

    #[inline(always)]
    fn get_backend_hdr<'a>(&self, branch: BranchName<'a>) -> DzkBackend<'a> {
        DzkBackend {
            branch,
            state: self.token.accounts.clone(),
            storages: self.token.storages.clone(),
            block_hashes: self.block_hashes,
            vicinity: self.vicinity.clone(),
        }
    }

    // update with each new block
    #[inline(always)]
    pub fn update_vicinity(
        &mut self,
        chain_id: U256,
        block_coinbase: H160,
        block_timestamp: U256,
    ) {
        self.vicinity = DzkVicinity {
            gas_price: self.gas_price.get_value().unwrap(),
            origin: H160::zero(),
            chain_id,
            block_number: U256::from(
                self.block_hashes.last().map(|(h, _)| h).unwrap_or(0),
            ),
            block_coinbase,
            block_timestamp,
            block_difficulty: U256::zero(),
            block_gas_limit: self.block_gas_limit.get_value().unwrap(),
            block_base_fee_per_gas: self.block_base_fee_per_gas.get_value().unwrap(),
        };
    }
}

impl Default for State {
    // NOTE:
    // Do NOT use `..Default::default()` style!
    // Using this style here will make your stack overflow.
    fn default() -> Self {
        Self::new()
    }
}

// Account information of a vsdb backend.
#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct DzkAccount {
    pub nonce: U256,
    pub balance: U256,
    pub code: Vec<u8>,
}

impl DzkAccount {
    pub fn from_balance(balance: U256) -> Self {
        Self {
            balance,
            ..Default::default()
        }
    }
}

#[derive(Vs, Default, Clone, Debug, Serialize, Deserialize)]
pub struct DzkVicinity {
    pub gas_price: U256,
    pub origin: H160,
    pub chain_id: U256,
    // Environmental block number.
    pub block_number: U256,
    // Environmental coinbase.
    // `H160(original proposer address)`
    pub block_coinbase: H160,
    // Environmental block timestamp.
    pub block_timestamp: U256,
    // Environmental block difficulty.
    pub block_difficulty: U256,
    // Environmental block gas limit.
    pub block_gas_limit: U256,
    // Environmental base fee per gas.
    pub block_base_fee_per_gas: U256,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CallContractResp {
    pub evm_resp: ExitReason,
    pub data: Vec<u8>,
    pub gas_used: u64,
}

fn snapshot_at_height(
    height: BlockHeight,
    evm_state: &State,
    prefix: &str,
) -> Result<String> {
    if height > 0 {
        let ver = VsVersion::new_with_default_mark(height, 0);

        let id = rand::random::<u64>();
        let ver_tmp = format!("{}_{}_{}", prefix, height, id);
        let branch_tmp = format!("{}_{}_{}", prefix, height, id);

        evm_state
            .branch_create_by_base_branch_version(
                BranchName::from(branch_tmp.as_str()),
                ver_tmp.encode_value().as_ref().into(),
                ParentBranchName::from(MAIN_BRANCH_NAME.0),
                ver.encode_value().as_ref().into(),
                false,
            )
            .c(d!(@ver))?;

        Ok(branch_tmp)
    } else {
        Err(eg!("block height cannot be 0"))
    }
}
