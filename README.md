![GitHub top language](https://img.shields.io/github/languages/top/Microverus/DZK)
![GitHub issues](https://img.shields.io/github/issues-raw/Microverus/DZK)
![GitHub pull requests](https://img.shields.io/github/issues-pr-raw/Microverus/DZK)
![GitHub Workflow Status](https://img.shields.io/github/workflow/status/Microverus/DZK/Main)
![Minimum rustc version](https://img.shields.io/badge/rustc-1.60+-lightgray.svg)

# DZK

### Documents

- [**Proposals**](./documents/proposal/README.md)
- [**Change log**](./documents/change_log.md)

### Front-End Usage

- **Wallet(Metamask) settings**
  - **Network Name**: Microverus Testnet
  - **New RPC URL**: https://prod-testnet.prod.microverus.io:8545
  - **Chain ID**: 9527
  - **Currency Symbol**: DZK
  - **Block Explorer URL**: https://prod-testnet-blockscout.prod.microverus.io
- **Blockchain browser(Blockscout)**
  - **url**: https://prod-testnet-blockscout.prod.microverus.io

### Command-Line Usage

Quick start:

```shell
# compile binaries
make
# start a pre-configed environment
make localnet_run
```

Top-level overview:

```shell
dzk
Official implementations of the Microverus project.

USAGE:
    dzk <SUBCOMMAND>

OPTIONS:
    -h, --help       Print help information
    -V, --version    Print version information

SUBCOMMANDS:
    cli       Run dzk in client mode
    daemon    Run dzk in daemon mode, aka run a node
    dev       Development utils, creating a local env, etc.
    help      Print this message or the help of the given subcommand(s)
```

A very useful sub-command for developers:

```shell
dzk-dev
Development utils, creating a local env, etc.

USAGE:
    dzk dev [OPTIONS]

OPTIONS:
    -a, --env-add-node
    -c, --env-create
        --chain-id <CHAIN_ID>             [default: 9527]
    -d, --env-destroy
    -h, --help Print help information
    -i, --env-info
    -I, --block-itv-secs <BLOCK_ITV_SECS> [default: 3]
        --initial-phrase <INITIAL_PHRASE>
    -n, --env-name <ENV_NAME>
    -N, --validator-num <VALIDATOR_NUM>   [default: 3]
    -r, --env-rm-node
    -s, --env-start
    -S, --env-stop
```

Generate a new account:

```shell
# dzk cli -g
Address: 0x0bab883d3adb7ec30f8b6ec9f3fc7265d20cae94
Phrase: face coach arrive affair gasp winner slow focus romance nothing project lesson
```

### Environment variables

- `${DZK_BENCH_SERV}`
  - Define this VAR if you want a custom web3-endpoint
  - Eg: 'https://dev-qa01.dev.microverus.io:8545'
  - Default value is 'http://localhost:8545'
- `${PRODENV_NODE_CNT}`
  - How many validator nodes do you want when creating new local cluster
  - Default value is 7
